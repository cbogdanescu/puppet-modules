About
=====

Puppet module for Taverna Server developed in the frame of the FP7-SCAPE project

Dependencies
============

Puppet

Documentation
=============

How to use
----------

::

    node node_name{
         class{'scape_taverna':
      $tomcatWebApps="TOMCAT_WEBAPPS_LOCATION",
      $tavernaLocation="TAVERNA_SERVER_ARCHIVE_LOCATION",
      $tomcatPort=TOMCAT_PORT
      }
    }

