About
=====

Puppet module for Apache Tomcat 7 developed in the frame of the FP7-SCAPE project

Dependencies
============

Puppet

Documentation
=============

How to use
----------

::

    node node_name{
         class{'scape_tomcat':
      $version="tomcat7",
      $port=TOMCAT_PORT,
      $jdk="JDK_LOCATION",
      $username="TOMCAT_USER",
      $password="TOMCAT_USER_PASSWORD",
      }
    }

