About
=====

Puppet module for SCAPE packages developed in the frame of the FP7-SCAPE
project

Dependencies
============

Puppet

Documentation
=============

How to use
----------

::

    node node_name{
        class{'scape_packages':}
    }

