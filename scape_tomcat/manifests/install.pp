/*
Copyright 2014 Universitatea de Vest din Timișoara

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author: Caius Bogdanescu <caius.bogdanescu@info.uvt.ro>
@contact: caius.bogdanescu@info.uvt.ro
@copyright: 2014 Universitatea de Vest din Timișoara
*/

# Class: scape_tomcat::install
#
# This class installs Apache Tomcat 7
#

class scape_tomcat::install{

 class{'java':}
  ->
   package { "tomcat7":
    ensure => "present",
  }
  ->
  package { "tomcat7-user":
    ensure  => present,
    require => Package["tomcat7"],
  }
  ->
  package { "tomcat7-admin":
    ensure  => present,
    require => Package["tomcat7-user"],
  }

}

