node scapecdh {

  apt::source { 'clouder-cdh4-repository':
    location     => 'http://archive.cloudera.com/cdh4/ubuntu/precise/amd64/cdh',
    release      => 'precise-cdh4',
    repos => 'contrib',
    architecture => 'amd64 ',
    key => '02A818DD',
    key_server => 'keys.gnupg.net',

    include_src  => false,
  }

  file { '/var/lib/hadoop/':
    ensure => "directory",
    owner => "hdfs"
  }

  file { '/var/lib/hadoop/data/':
    ensure => "directory",
    owner => "hdfs",
    require => File["/var/lib/hadoop/"]
  }

  file { '/usr/lib/hadoop-hdfs/libexec':
    ensure => "link",
    target => "/usr/lib/hadoop/libexec/"
  }

  file {"/usr/lib/hadoop/logs":
    ensure => "directory",
    owner => "hdfs"
  }

  class {"java":
    version => "java6"
  }

  class { 'cdh4::hadoop':
    # Must pass an array of hosts here, even if you are
    # not using HA and only have a single NameNode.
    namenode_hosts     => ['scape-hadoop-namenode.local'],
    datanode_mounts    => [
                           '/var/lib/hadoop/data/a',
                           '/var/lib/hadoop/data/b',
                           '/var/lib/hadoop/data/c'
                           ],
    # You can also provide an array of dfs_name_dirs.
    dfs_name_dir       => '/var/lib/hadoop/name',
    notify => [File["/var/lib/hadoop/"], File["/usr/lib/hadoop-hdfs/libexec"], File["/usr/lib/hadoop/logs/"]],
    require => [Class["java"], ]
  }
  
  Host <<| |>>
}

node hadoop_server inherits scapecdh {
  include cdh4::hadoop::master
  #class { 'cdh4::hive::master': }
  @@host { "$ec2_hostname":
    ip => "$ec2_local_ipv4",
    host_aliases => ["$hostname", "scape-hadoop-namenode.local"],
  }
}


node hadoop_worker inherits scapecdh {
  include cdh4::hadoop::worker
  @@host { "$ec2_hostname":
    ip => "$ec2_local_ipv4",
    host_aliases => "$hostname",
  }
}
